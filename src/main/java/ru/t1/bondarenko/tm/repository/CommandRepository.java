package ru.t1.bondarenko.tm.repository;

import ru.t1.bondarenko.tm.api.repository.ICommandRepository;
import ru.t1.bondarenko.tm.constant.ArgumentConstant;
import ru.t1.bondarenko.tm.constant.TerminalConstant;
import ru.t1.bondarenko.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.INFO,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.HELP,
            "Show application commands."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, ArgumentConstant.COMMANDS,
            "Show application commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS,
            "Show application arguments."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null,
            "Close application."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConstant.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConstant.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConstant.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConstant.PROJECT_SHOW_BY_ID, null, "Show Project by ID."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConstant.PROJECT_SHOW_BY_INDEX, null, "Show Project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_ID, null, "Update Project by ID."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_INDEX, null, "Update Project by index."
    );

    private static final Command PROJECT_CLEAR_BY_ID = new Command(
            TerminalConstant.PROJECT_CLEAR_BY_ID, null, "Delete Project by ID."
    );

    private static final Command PROJECT_CLEAR_BY_INDEX = new Command(
            TerminalConstant.PROJECT_CLEAR_BY_INDEX, null, "Delete Project by index."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConstant.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConstant.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConstant.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConstant.TASK_SHOW_BY_ID, null, "Show Task by ID."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConstant.TASK_SHOW_BY_INDEX, null, "Show Task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConstant.TASK_UPDATE_BY_ID, null, "Update Task by ID."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConstant.TASK_UPDATE_BY_INDEX, null, "Update Task by index."
    );

    private static final Command TASK_CLEAR_BY_ID = new Command(
            TerminalConstant.TASK_CLEAR_BY_ID, null, "Delete Task by ID."
    );

    private static final Command TASK_CLEAR_BY_INDEX = new Command(
            TerminalConstant.TASK_CLEAR_BY_INDEX, null, "Delete Task by index."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, VERSION, HELP, COMMANDS, ARGUMENTS,

            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_CLEAR_BY_ID, PROJECT_CLEAR_BY_INDEX,

            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_CLEAR_BY_ID, TASK_CLEAR_BY_INDEX,

            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    };

}

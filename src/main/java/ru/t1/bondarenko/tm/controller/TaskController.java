package ru.t1.bondarenko.tm.controller;

import ru.t1.bondarenko.tm.api.controller.ITaskController;
import ru.t1.bondarenko.tm.api.service.ITaskService;
import ru.t1.bondarenko.tm.model.Project;
import ru.t1.bondarenko.tm.model.Task;
import ru.t1.bondarenko.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        final List<Task> projects = taskService.findAll();
        int index = 1;
        for (final Task task : projects) {
            final String name = task.getName();
            final String description = task.getDescription();
            System.out.printf("%s. %s : %s \n", index, name, description);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.deleteAll();
        System.out.println("[OK]");
    }

    @Override
    public void clearTaskByID() {
        System.out.println("[DELETE TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.clearByID(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearTaskByIndex() {
        System.out.println("[DELETE TASK BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.clearByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void showTaskByID() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.clearByID(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.clearByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByID() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByID(id, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}

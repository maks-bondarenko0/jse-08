package ru.t1.bondarenko.tm.service;

import ru.t1.bondarenko.tm.api.repository.IProjectRepository;
import ru.t1.bondarenko.tm.api.service.IProjectService;
import ru.t1.bondarenko.tm.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public void deleteAll() {
        projectRepository.deleteAll();
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return projectRepository.add(project);
    }

    @Override
    public Project findByID(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findByID(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project delete(final Project project) {
        if (project == null) return null;
        return projectRepository.delete(project);
    }

    @Override
    public Project clearByID(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.clearByID(id);
    }

    @Override
    public Project clearByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.clearByIndex(index);
    }

    @Override
    public Project updateByID(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        Project project = projectRepository.findByID(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        Project project = projectRepository.findByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}

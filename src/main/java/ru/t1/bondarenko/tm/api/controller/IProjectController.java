package ru.t1.bondarenko.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

    void clearProjectByID();

    void clearProjectByIndex();

    void showProjectByID();

    void showProjectByIndex();

    void updateProjectByID();

    void updateProjectByIndex();

}

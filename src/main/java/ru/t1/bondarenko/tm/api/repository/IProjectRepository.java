package ru.t1.bondarenko.tm.api.repository;

import ru.t1.bondarenko.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void deleteAll();

    Project findByID(String id);

    Project findByIndex(Integer index);

    Project delete(Project project);

    Project clearByID(String id);

    Project clearByIndex(Integer index);

}
